/* eslint-disable no-use-before-define */

import KeenSlider from 'keen-slider';

(() => {
  const sliders = document.querySelectorAll('.keen-slider');

  if (!sliders) return;

  sliders.forEach((slider) => {
    const slides = slider.querySelectorAll('.keen-slider-slide');

    // update dots
    const updateDots = (instance) => {
      const slide = instance.details().relativeSlide;
      const dots = slider.querySelectorAll('.dot');
      dots.forEach((dot, index) => {
        index === slide
          ? dot.classList.add('dot-active')
          : dot.classList.remove('dot-active');
      });
    };

    // autoplay
    let interval;

    const autoplay = (run) => {
      clearInterval(interval);

      interval = setInterval(() => {
        if (run && sliderInstance) sliderInstance.next();
      }, 4000);
    };

    // initialization
    const sliderInstance = new KeenSlider(slider, {
      duration: 2000,
      loop: true,
      slides: slides.length,
      dragStart: () => autoplay(false),
      dragEnd: () => autoplay(true),
      slideChanged: (instance) => updateDots(instance),
      created: (instance) => {
        const dots = slider.querySelector('.dots');
        slides.forEach((element, index) => {
          const dot = document.createElement('button');
          dot.classList.add('dot');
          dot.setAttribute('aria-label', `Buttonlink zu Slide ${index + 1}`);
          dots.appendChild(dot);
          dot.addEventListener('click', () => instance.moveToSlide(index, 0));
        });
        updateDots(instance);
      },
      move: (element) => {
        const opacities = element.details().positions.map((position) => position.portion);
        slides.forEach((slide, index) => {slide.style.opacity = opacities[index];});
      },
    });

    autoplay(true);
  });
})();
