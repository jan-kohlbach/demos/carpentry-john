import LocomotiveScroll from 'locomotive-scroll';

(() => {
  if (!document.querySelector('[data-scroll-container]')) return;

  const scroll = new LocomotiveScroll({
    el: document.querySelector('[data-scroll-container]'),
    smooth: true,
    lerp: 0.03,
  });

  scroll.destroy();

  window.addEventListener('load', () => scroll.init());
})();
