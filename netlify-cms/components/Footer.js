import React from 'react';

const Footer = () => (
  <footer>
    <div className="space"></div>
    <div className="links">
      <a href="/legal-notice">Legal Notice</a>
      <a href="/privacy-policy">Privacy Policy</a>
    </div>
    <div className="madeby">
      <span>made by <a href="https://jankohlbach.com">jankohlbach.com</a></span>
    </div>
  </footer>
);

export default Footer;
