import React, {useEffect} from 'react';
import {Remarkable} from 'remarkable';
import parse from 'html-react-parser';

import Footer from '../components/Footer';

const HomepagePreview = ({widgetsFor, document}) => {
  const md = new Remarkable();

  useEffect(() => {
    const script = document.createElement('script');
    script.src = '../main.js';
    script.setAttribute('defer', '');
    document.head.appendChild(script);
  }, []);

  return (
    <div data-scroll-container="">
      <main>
        {widgetsFor('components').map((component, i) => {
          const get = (key) => component.getIn(['data', key]);

          const type = get('type');
          const textItem = get('content')?.toJS()[0];
          const mediaItem = get('media')?.toJS()[0];

          switch (type) {
            case 'hero':
              return (
                <div className="hero" key={i}>
                  <div className="title-wrapper">
                    <h1>
                      <span className="title" data-scroll="" data-scroll-speed="2">{get('title')}</span>
                      <span className="title title-sub" data-scroll="" data-scroll-speed="2">{get('titleSub')}</span>
                    </h1>
                  </div>
                  <div className="subline-wrapper" data-scroll="" data-scroll-speed="2">
                    <span className="subline subline-primary">{get('subPrimary')}</span>
                    <span className="subline subline-secondary">{get('subSecondary')}</span>
                  </div>
                  <div className="keen-slider">
                    {get('slider')?.toJS().map((obj, j) => {
                      switch (obj?.type) {
                        case 'image':
                          return (
                            <div className="keen-slider-slide" key={j}>
                              <img src={obj?.imgSrc} alt={obj?.alt}/>
                            </div>
                          );
                        case 'video':
                          return (
                            <div className="keen-slider-slide" key={j}>
                              <video
                                src={obj?.videoSrc}
                                muted
                                autoPlay
                                loop
                                playsInline
                                disablePictureInPicture
                              />
                            </div>
                          );
                        default:
                          return null;
                      }
                    })}
                    <div id="dots" className="dots" />
                  </div>
                </div>
              );
            case 'textMedia':
              return (
                <div className="container content-wrap" key={i}>
                  <div className={`text-media${get('gallery') ? ' has-gallery' : ''}${get('reverse') ? ' reverse' : ''}${get('short') ? ' short' : ''}`}>
                    <div className="text" data-scroll="" data-scroll-class="in-view">
                      {get('title') && <h2>{get('title')}</h2>}
                      {textItem?.type === 'content-text' && parse(md.render(textItem.text))}
                      {textItem?.type === 'content-contact' && (
                        <div className="contact">
                          {textItem?.title && <span className="title">{textItem.title}</span>}
                          {textItem?.subTitle && <span className="title title-sub">{textItem.subTitle}</span>}
                          {textItem?.rest && parse(md.render(textItem.rest))}
                          {textItem?.phone && <span>Phone: <a href={`tel:${textItem.phoneLink}`}>{textItem.phone}</a></span>}
                          {textItem?.mail && <span>E-Mail: <a href={`mailto:${textItem.mail}`}>{textItem.mail}</a></span>}
                        </div>
                      )}
                    </div>
                    <div className="media" data-scroll="" data-scroll-class="in-view" data-scroll-speed="1">
                      {mediaItem?.type === 'image' && (
                        <div className="image">
                          <img src={mediaItem.imgSrc} alt={mediaItem.alt}/>
                        </div>
                      )}
                      {mediaItem?.type === 'video' && (
                        <div className="video">
                          <video
                            src={mediaItem.videoSrc}
                            muted
                            autoPlay
                            loop
                            playsInline
                            disablePictureInPicture
                          />
                        </div>
                      )}
                      {mediaItem?.type === 'gallery' && (
                        <div className="gallery">
                          {mediaItem.images?.map((obj, j) => {
                            const lightbox = obj?.image?.lightbox;
                            const preview = obj?.image?.preview;

                            return (
                              <a href={lightbox?.imgSrc} className="glightbox" key={j}>
                                <img src={preview?.imgSrc} alt={preview?.alt}/>
                              </a>
                            );
                          })}
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              );
            default:
              return null;
          }
        })}
      </main>
      <Footer />
    </div>
  );
};

export default HomepagePreview;
