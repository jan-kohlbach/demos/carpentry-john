import React, {useEffect} from 'react';

import Footer from '../components/Footer';

const PagePreview = ({widgetFor, document}) => {
  useEffect(() => {
    const script = document.createElement('script');
    script.src = '../main.js';
    script.setAttribute('defer', '');
    document.head.appendChild(script);
  }, []);

  return (
    <div data-scroll-container="">
      <header>
        <div className="content-wrap">
          <a href="/" className="logo">
            <span className="primary">carpentry</span>
            <span className="secondary">john</span>
          </a>
        </div>
      </header>
      <main>
        <div className="content-wrap">
          {widgetFor('content')}
        </div>
      </main>
      <Footer />
    </div>
  );
};

export default PagePreview;
