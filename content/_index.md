---
title: ""
date: 2020-12-27T12:31:14+01:00
draft: false
components:
  - type: hero
    title: carpentry
    titleSub: john
    subPrimary: john miller
    subSecondary: Your partner for furniture, interior work and individual solutions.
    slider:
      - type: image
        imgSrc: /uploads/carpenter-1.jpg
        alt: carpenter 1
      - type: image
        imgSrc: /uploads/carpenter-2.jpg
        alt: carpenter 2
      - type: image
        alt: carpenter 3
        imgSrc: /uploads/carpenter-3.jpg
      - type: image
        alt: carpenter 4
        imgSrc: /uploads/carpenter-4.jpg
      - type: image
        alt: carpenter 5
        imgSrc: /uploads/carpenter-5.jpg
      - type: image
        alt: carpenter 6
        imgSrc: /uploads/carpenter-6.jpg
  - type: textMedia
    short: true
    content:
      - type: content-text
        text: >-
          Carpentry John - Your professional partner for furniture, interior
          work and and individual solutions.


          We're looking forward to your projects and new challenges!


          John Miller & Team
    media:
      - type: image
        imgSrc: /uploads/carpenter-3.jpg
        alt: carpenter 3
  - type: textMedia
    reverse: true
    title: John Miller
    content:
      - type: content-text
        text: >-
          Here goes some text about John. Yeah he's imaginary, but who cares. He
          for sure has an interesting story behind this business. Here's some
          notes.


          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
    media:
      - type: image
        imgSrc: /uploads/carpenter-4.jpg
        alt: carpenter 4
  - type: textMedia
    title: Services
    content:
      - type: content-text
        text: >-
          Everything included here. We're happy to advice you and make you an
          indiual offer in the following areas:


          * All-included interior work

          * Shop interior work

          * Furniture for museums, galleries, fairs

          * Special demands

          * Unique pieces

          * Competent planning
    media:
      - type: image
        imgSrc: /uploads/carpenter-5.jpg
        alt: carpenter 5
  - type: textMedia
    short: true
    reverse: true
    title: References
    content:
      - type: content-text
        text: Here's a short overview of our work.
    media:
      - type: gallery
        images:
          - image:
              preview:
                imgSrc: /uploads/carpenter-1.jpg
                alt: carpenter 1
              lightbox:
                imgSrc: /uploads/carpenter-1.jpg
                alt: carpenter 1
          - image:
              preview:
                imgSrc: /uploads/carpenter-2.jpg
                alt: carpenter 2
              lightbox:
                imgSrc: /uploads/carpenter-2.jpg
                alt: carpenter 2
          - image:
              preview:
                imgSrc: /uploads/carpenter-3.jpg
                alt: carpenter 3
              lightbox:
                imgSrc: /uploads/carpenter-3.jpg
                alt: carpenter 3
          - image:
              preview:
                imgSrc: /uploads/carpenter-4.jpg
                alt: carpenter 4
              lightbox:
                imgSrc: /uploads/carpenter-4.jpg
                alt: carpenter 4
          - image:
              preview:
                imgSrc: /uploads/carpenter-5.jpg
                alt: carpenter 5
              lightbox:
                imgSrc: /uploads/carpenter-5.jpg
                alt: carpenter 5
          - image:
              preview:
                imgSrc: /uploads/carpenter-6.jpg
                alt: carpenter 6
              lightbox:
                imgSrc: /uploads/carpenter-6.jpg
                alt: carpenter 6
  - type: textMedia
    short: true
    title: Contact
    content:
      - type: content-contact
        title: CARPENTRY JOHN
        subTitle: john miller
        rest: |-
          Street 423\
          12345 City
        phoneLink: 123456789
        mail: test@test.com
        phone: 01234 - 56789
    media:
      - type: image
        imgSrc: /uploads/carpenter-1.jpg
        alt: carpenter 1
---
